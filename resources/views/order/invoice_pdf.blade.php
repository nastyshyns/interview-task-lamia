<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Lamia</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
    </head>
    <body>
		<center><h1>Order Details</h1></center>
		<p><b>Custumer first name:</b> {{ $customer_first_name }}</p>
		<p><b>Custumer last name:</b> {{ $customer_last_name }}</p>
		<center><b>Goods</b></center>
		<table>
			<tr>
				<th>Name</th>
				<th>Quantity</th>
				<th>Price, $</th>
				<th>VAT, %</th>
			</tr>
			@foreach($products as $product)
				<tr>
					<td> {{ $product['name'] }}</td>
					<td> {{ $product['quantity'] }}</td>
					<td> {{ $product['price'] }}</td>
					<td> {{ $product['vat'] }}</td>
				</tr>
			@endforeach
		</table>
		<p><b>Subtotals</b></p>
		<ul>
			@foreach($subtotals as $subtotal_name => $subtotal)
				<li><b> {{ $subtotal_name }}: </b> ${{ $subtotal }} </li>
			@endforeach
		</ul>
		<p><b><u>Totaly: </u> {{ $total }}</b></p>
		
    </body>
</html>
