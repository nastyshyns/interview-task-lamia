<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Lamia</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
    </head>
    <body>
        <h1>Dear, Mr/Mrs {{ $customer_last_name }}</h1>
		<p> Your order is being processed </p>
		<h2>Order Details</h2>
		<center><b>Goods</b></center>
		<table>
			<tr>
				<th>Name</th>
				<th>Quantity</th>
				<th>Price, $</th>
				<th>VAT, %</th>
			</tr>
			@foreach($products as $product)
				<tr>
					<td> {{ $product['name'] }}</td>
					<td> {{ $product['quantity'] }}</td>
					<td> {{ $product['price'] }}</td>
					<td> {{ $product['vat'] }}</td>
				</tr>
			@endforeach
		</table>
		<p><b>Subtotals</b></p>
		<ul>
			@foreach($subtotals as $subtotal_name => $subtotal)
				<li><b> {{ $subtotal_name }}: </b> ${{ $subtotal }} </li>
			@endforeach
		</ul>
		<p><b><u>Totaly: </u> {{ $total }}</b></p>
		
    </body>
</html>
