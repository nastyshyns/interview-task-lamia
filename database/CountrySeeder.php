<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(file_exists(base_path(). '/'. 'countries.json')){
			$countries_json = file_get_contents(base_path(). '/'. 'countries.json');
			$countries=json_decode($countries_json);
			foreach($countries as $country){
				if($country->name){
					DB::table('countries')->insert([
						"title" => $country->name,
						"created_at" =>  \Carbon\Carbon::now(),
						"updated_at" => \Carbon\Carbon::now()
					]);
				}
			}
			
		}
    }
}
