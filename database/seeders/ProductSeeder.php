<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$i=0;
		while($i<100)
		{
			$product_id = DB::table('products')->insertGetId([
				'product_name'  => 'product-'.$i.' vat: Poland, Ukraine, Finland',
				'price'        => rand(10,10000),
				"created_at" =>  \Carbon\Carbon::now(),
				"updated_at" => \Carbon\Carbon::now()
			]);
			
			DB::table('product_taxes')->insert([
				'product_id'    => $product_id,
				'country_id'    => 175 ,  /*Poland*/
				'vat'           => rand(20,30),
				"created_at" =>  \Carbon\Carbon::now(),
				"updated_at" => \Carbon\Carbon::now()
			]);
			
			
			DB::table('product_taxes')->insert([
				'product_id'    => $product_id,
				'country_id'    => 73 ,   /*Finland*/
				'vat'           => rand(20,30),
				"created_at" =>  \Carbon\Carbon::now(),
				"updated_at" => \Carbon\Carbon::now()
			]);
			
			DB::table('product_taxes')->insert([
				'product_id'    => $product_id,
				'country_id'    => 227 ,   /*Ukraine*/
				'vat'           => rand(20,30),
				"created_at" =>  \Carbon\Carbon::now(),
				"updated_at" => \Carbon\Carbon::now()
			]);
			
			$i++;
			
			
		}
		
		while($i<200)
		{
			$product_id = DB::table('products')->insertGetId([
				'product_name'  => 'product-'.$i.' vat: Poland, Finland',
				'price'        => rand(10,10000),
				"created_at" =>  \Carbon\Carbon::now(),
				"updated_at" => \Carbon\Carbon::now()
			]);
			
			DB::table('product_taxes')->insert([
				'product_id'    => $product_id,
				'country_id'    => 175  ,  /*Poland*/
				'vat'           => rand(20,30),
				"created_at" =>  \Carbon\Carbon::now(),
				"updated_at" => \Carbon\Carbon::now()
			]);
			
			DB::table('product_taxes')->insert([
				'product_id'    => $product_id,
				'country_id'    => 73  ,  /*Finland*/
				'vat'           => rand(20,30),
				"created_at" =>  \Carbon\Carbon::now(),
				"updated_at" => \Carbon\Carbon::now()
			]);
			
			$i++;
			
		}
		
		
		while($i<300)
		{
			$product_id = DB::table('products')->insertGetId([
				'product_name'  => 'product-'.$i.' vat: Finland',
				'price'        => rand(10,10000),
				"created_at" =>  \Carbon\Carbon::now(),
				"updated_at" => \Carbon\Carbon::now()
			]);
			
						
			DB::table('product_taxes')->insert([
				'product_id'    => $product_id,
				'country_id'    => 73 ,   /*Finland*/
				'vat'           => rand(20,30),
				"created_at" =>  \Carbon\Carbon::now(),
				"updated_at" => \Carbon\Carbon::now()
			]);
			
			
			$i++;
		}
    }
}
