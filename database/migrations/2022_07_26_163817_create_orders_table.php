<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
			$table->string('customer_first_name');
			$table->string('customer_last_name');
			$table->string('customer_email');
			$table->string('invoice_format')->comment("JSON, HTML, ...");
			$table->boolean('email_confirmation')->comment("1 if the invoice should be sent to email, 2 if the invoice shouldn't be sent to the email");
			$table->text('comment');
			$table->integer('country_id')->unsigned();
			$table->json('subtotals');
			$table->float('total')->unsigned();
            $table->timestamps();
        });	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('order_products');
        Schema::dropIfExists('orders');
    }
}
