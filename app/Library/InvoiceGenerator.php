<?php
/**

* The class duty is to generate the invoices of various types and store it in memory.
* Part of the strategy patern, depence on the type of the invoice

*/

namespace App\Library;
use PDF;
use App\Models\Product;


class InvoiceGenerator 
{
	public $order_data;
	
	/**
	
	* In the constructor, the data of order is put in class property order_data
	
	*@param order_data assotiative array of the order details that are stored in database 
	
	*/
	
	function __construct($order_data)
	{
		$this->order_data   = $order_data;
		
	}
	
	/**
	
	* The method generates the invoice in JSON format and returns ['data-type' => 'json', 'content' => json-type of the invoice]
	
	*@return type: array
	
	*/
	
    public function generateJSON()
	{
		$invoice_data = json_encode($this->order_data);
		return ['data-type' => 'json', 'content' => $invoice_data,];
		
		
	}
	
	/**
	
	* The method generates the invoice in PDF format and returns ['data-type' => 'json', 'content' => resource of pdf-file with the invoice]
	
	*@return type: array
	
	*/
	
	public function generatePDF()
	{
		
		$order_data = $this->order_data;
		foreach($order_data['products'] as $key => $product){
			
			$product_data = Product::getProductWithTax($product['product_id'],$order_data['country_id']);
			$order_data['products'][$key]['name']  = $product_data->product_name;
			$order_data['products'][$key]['price'] = $product_data->price;
			$order_data['products'][$key]['vat']   = $product_data->vat;
		}
		
				
		$pdf = PDF::loadView('order.invoice_pdf', $order_data)->output();
		
		return ['data-type' => 'pdf', 'content' => $pdf];
		
	}
}
