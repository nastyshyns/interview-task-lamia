<?php

/**

* The Eloquent model class (table: Orders).
* The class calculates the subtotals, and total of the order, stores the data in DB and sends the e-mail notification to the customer

*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\ConfirmCustomer;

class Order extends Model
{
	
		
	/**
	
	 * The method calculates the total and subtotals of order and, then, stores the data of order  in DB
	
	 *@param order_data - data of the order type: associative array
	 
	 *@return associative array of stored data
	
	*/
    public function store(array $order_data)
	{
		//calculation of subtotals (could called as property container patern)
		$order_data['subtotals'] = [
			'products_price' => $this->calculate_products_total($order_data['products']),
			'products_tax'   => $this->calculate_products_tax($order_data['products'],$order_data['country_id'])
			//May be extended (for example the discounts, delivery, and etc.)
		];
		
		$order_data['total']     = array_sum($order_data['subtotals']); 
		
		//preparing the data of order for db
		$order_data_db = [
		
			'customer_first_name'  => $order_data['customer_first_name'],
			'customer_last_name'   => $order_data['customer_last_name'],
			'customer_email'       => (isset($order_data['customer_email'])) ? $order_data['customer_email'] : '',
			'country_id'           => $order_data['country_id'],
			'invoice_format'       => $order_data['invoice_format'],
			'email_confirmation'   => $order_data['email_confirmation'],
			'comment'              => $order_data['comment'] ?? '',   //not mentioned in the story, but usually custumer should have the ability to leave the comment 
			'total'                => $order_data['total'],
			'subtotals'            => json_encode($order_data['subtotals']),
			'created_at'           => Carbon::now(),
			'updated_at'           => Carbon::now()
			
		];
		$order_data['id'] = $order_id = $this->insertGetId($order_data_db);
		
		$order_products_db = [];
		foreach($order_data['products'] as $product){
			
			$order_products[] = [
				'order_id'    => $order_id,
				'product_id'  => $product['id'],
				'quantity'    => $product['quantity']
			];
			
		}
		DB::table('order_products')->insert($order_products);
		
		$order_data['products'] = $order_products;
				
		return $order_data;
		
	
	}
	
	
	/**
	 * The method calculates the total price of the products (without tax)
	
	 *@param products array of the products
	 
	 *@return total price of the products
	
	*/
	
	public function calculate_products_total(array $products)
	{
		$products_price = 0;
		foreach($products as $product)
		{
			$product_quantity       = floatval($product['quantity']);
			$product_price          = floatval(Product::find($product['id'])->price);
			$products_price         = $products_price + ($product_price*$product_quantity); 
		}
		
		return $products_price;
	}
	
	/**
	 * The method calculates the total tax of the products
	
	 *@param products array of the products
	 
	 
	 *@param country_id
	 
	 *@return total tax of the products
	
	*/
	
	public function calculate_products_tax($products, int $country_id)
	{
		
		$products_tax = 0;
		foreach($products as $product)
		{
			$product_quantity       = floatval($product['quantity']);
			$product_with_taxes     = Product::getProductWithTax($product['id'],$country_id);
			$product_price          = $product_with_taxes->price;
			$product_vat            = $product_with_taxes->vat;
			$products_tax           = $products_tax + ($product_price*$product_vat*$product_quantity)/100;
			
		}
		
		return $products_tax;
		
	}
	
	
	/**
	
	
	 * The method sends the invoice to the customer (the .env shoud be correct)
	 
	
	 *@param order_data
	 
	 
	 *@param  invoice is the generated resource of the invoise that to be sent to customer e-mail
	 
	 
	 *@return :mixed in case of non-correct setting of mail-system returns the messege of the error; if the mail was sent the method returns true;
	 
	
	*/
	
	public function notify_customer($order_data, $invoice)
	{
		
		$user = new User();
		$user->email = $order_data['customer_email'];
		$user->name = $order_data['customer_first_name'] . " " . $order_data['customer_last_name'];
		$success = true;
		
		try{
			Mail::to($user, $invoice)->send(new ConfirmCustomer($order_data, $invoice));
		} catch (\Exception $e){
			$success = $e->getMessage();
		}
		
		return $success;
		
	}
	 
	

}
