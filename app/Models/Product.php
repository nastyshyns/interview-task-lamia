<?php

/**

* The Eloquent model class (table: products).

*/

namespace App\Models;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	
	/**
	
	* The method gets the data of the corresponding product with taxes in corresponding country
	
	*@param product_id:int - id of product for getting the data from DB
	*@param country_id:int - id of the corresponding country
	
	*@return oblect with data of the product and tax in coresponding country
	
	*/
	public static function getProductWithTax(int $product_id, int $country_id)
	{
		$result = DB::select("
				SELECT * FROM `products` p 
				INNER JOIN `product_taxes` pt ON p.id=pt.product_id 
				WHERE p.id = ? AND pt.country_id = ? 
				LIMIT 1
			", [$product_id,$country_id]);
		
		return $result[0];
	}
	
	
    
}
