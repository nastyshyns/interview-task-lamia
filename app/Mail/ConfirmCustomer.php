<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use \App\Models\Product;

class ConfirmCustomer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Creates a new message instance.
     *
     * @return void
     */
	
	private $order_data;
	
    public function __construct($order_data, $invoice)
    {
      $order_data['subtotals']   = $order_data['subtotals'];
      $this->order_data          = $order_data;
      $this->subject             = "Confirmation of your order";
      $this->name                = env("APP_NAME");  //Actually, in 80% of the cases the mailing services ignore it, and it should be im the mailing system

		$this->attachData($invoice['content'], 'invoice.'.$invoice['data-type']);
    }

    /**
     * Build the message.
     *
     * @return the e-mail the has to be sent
     *
     */
    public function build()
    {
		$order_data = $this->order_data;
		foreach($order_data['products'] as $key => $product)
		{
			$product_data = Product::getProductWithTax($product['product_id'],$order_data['country_id']);
			
			$order_data['products'][$key]['name']  = $product_data->product_name;
			$order_data['products'][$key]['price'] = $product_data->price;
			$order_data['products'][$key]['vat']   = $product_data->vat;
		}
			
        return $this->view('order.order_confirmation', $order_data);
    }
}
