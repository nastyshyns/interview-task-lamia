<?php

/**

* The class contains the logic of the making of the new order

* 
*/

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Support\Facades\Validator;
use App\Library\InvoiceGenerator;

class OrderController extends Controller
{

    /**
     * The class run the creation of the new order and reneration of the invoice (controller of MVC patern)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
        $order_data = $request_fields = $request->all();
		$errors = Validator::make($request_fields,[
			'products'                  => 'required|array',
			'products.*.id'             => 'required|distinct|exists:products,id|exists:product_taxes,product_id,country_id,'.$request_fields['country_id'],
			'products.*.quantity'       => 'required|integer',
			'country_id'                => 'required|exists:countries,id',
			'email_confirmation'        => 'required|in:1,0',
			'customer_email'            => 'required_if:email_confirmation,1|email',
			'customer_first_name'       => 'required|alpha',
			'customer_last_name'        => 'required|alpha',
			'invoice_format'            => 'required|in:JSON,PDF'
		])->errors()->all();
		

		if($errors){
			return response()->json($errors,400);
		}

		
		$order = new Order();
		$order_data = $order->store($order_data);
		/* STRATEGY PATTERN*/
		switch ($order_data['invoice_format']){
			case 'JSON' :
				$invoice = (new InvoiceGenerator($order_data))->generateJSON();
				$responce_headers['Content-Type'] = 'application/json';
				break;
			case 'PDF' :
				$invoice = (new InvoiceGenerator($order_data))->generatePDF($order_data);
				$responce_headers['Content-Type'] = 'application/pdf';
				break;			
		}
		/* \STRATEGY PATTERN*/
		
		if($order_data['email_confirmation'] == 1) {			
			$mail_sent = $order->notify_customer($order_data, $invoice);
			
			if($mail_sent !== true) {
				
				$errors[] = $mail_sent;
				return response()->json($errors,500);
			}
		}
		
		
		
		return response($invoice['content'],200)->withHeaders($responce_headers);
		
		
    }
}
