<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//API route 
Route::post('orders', 'App\Http\Controllers\Api\OrderController@store');
//FoR FULL API METHOD `post` MAY BE CHANGED TO `resource` (see laravel documentation)